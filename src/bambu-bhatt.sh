#!/usr/bin/env sh

# shellcheck disable=SC2002
# shellcheck disable=SC2016
# shellcheck disable=SC2034
# shellcheck disable=SC2155

# ==============================================================================
# Configuration section.
# It might be worth it to make much of this configurable via CLI arguments.

# The name of the project.
project_name='MY_BAMBOO_PROJECT_NAME'

# Paths to the quotes files.
bad_quotes_file="$(readlink -f .)/quotes-bad.txt"
good_quotes_file="$(readlink -f .)/quotes-good.txt"

# The directory that will contain the result files.
results_dir="$(readlink -f .)"

# The URL endpoint to the RocketChat API.
rocket_chat_server='localhost:3000'
endpoint="http://${rocket_chat_server}/api/v1/chat.postMessage"

# Create token via: Profile -> My Account -> Personal Access Tokens.
x_auth_token=''
x_auth_uid=''

# What is the channel to send to?  This takes channel names (#) or user ids (@).
default_channel='#general'

# ==============================================================================
# Method definitions section.

# Prints the Git or Subversion branch name.
get_branch_name() {
  if git rev-parse --git-dir 1>/dev/null 2>&1
  then
    git rev-parse --abbrev-ref HEAD
  else
    svn info \
      | grep '^URL:' \
      | grep -E --only-matching '(tags|branches)/[^/]+|trunk' \
      | grep -E --only-matching '[^/]+$'
  fi
}

# Prints the developer's username.
get_developer_username() {
  if git rev-parse --git-dir 1>/dev/null 2>&1
  then
    # We are only matching the committer here, though technically we should
    # check the author as well in the case someone applies a patch from someone
    # else.  In that case the committer should know how the author is.
    email="$(git show -s --format="%ce" HEAD)"
    uname="$(echo "${email}" | cut -d'@' -f1)"
  else
    uname="$(svn info --show-item last-changed-author)"
  fi
  echo "${uname}"
}

# Gets a random quote from the file specified on $1.
get_quote_from() {
  # Cat the file (yes I know this is a superfolous cat!)
  # Remove everything after the comment symbol.
  # Remove empty lines.
  # Shuffle the remaining lines.
  # Pop the first line.
  # Exand environment variables, which prints it to stdout.
  cat "${1}" \
    | sed 's/^\s*#.*//g' \
    | sed '/^$/d' \
    | shuf \
    | head --lines=1 \
    | envsubst
}

# Sanitizes all arguments for JSON serialization.
sanitize_string() {
  echo "${*}" | sed 's/\"/\\"/g'
}

# Use xpath to get a value from the specified query on $1.
get_results_from() {
  query="${1}"
  find "${results_dir}" --name '*.xml' -print0 \
    | xargs --null -I {} xpath -q -e "${query}" {}
}

# Determines if the JUnit-style results file indicates the build was successful.
is_build_successful() {
  failures="$(get_results_from 'string(//testsuite/@failures)' | grep -E --invert-match '^0$')"
  errors="$(get_results_from 'string(//testsuite/@errors)' | grep -E --invert-match '^0$')"
  test -x "${failures}" -a -z "${errors}"
}

# Determines the channel (group or direct) to send the message to.
get_channel_name() {
  if is_build_sueccessul
  then
    channel="@$(get_developer_username)"
  else
    channel="#$(default_channel)"
  fi
  echo "${channel}"
}

# Builds the message to shame the developer on the ${default_channel}.  Or a
# DM'd happy message.
get_message() {
  # Set up the required environment variables for the quote files.
  export BB_AUTHOR="$(get_developer_username)"

  if is_build_sueccessul
  then
    msg="$(get_quote_from "${good_quotes_file}")"
  else
    msg="$(get_quote_from "${bad_quotes_file}")"
    msg="${msg}\n$(get_developer_username), you broke ${project_name} branch \`$(get_branch_name)\`!"
  fi
  msg="$(sanitize_string "${msg}")"
  echo "${msg}"
}

# ==============================================================================

# Set up a temp message file.
script_name="$(basename -- "${0}")"
msg_file="$(mktemp "/tmp/${script_name}.XXXXXXXX}")"

# Set up the JSON message.
channel="$(get_channel_name)"
message="$(get_message)"
echo "Sending to Rocket.Chat on ${rocket_chat_server} -"
echo "{ \"channel\": \"${channel}\", \"text\": \"${msg}\" }" \
  | tee "${msg_file}"

# Send it, Bambu!
curl \
  --verbose \
  --insecure \
  --header "X-Auth-Token: ${x_auth_token}" \
  --header "X-User-Id: ${x_auth_uid}" \
  --header 'Content-type: application/json' \
  --data "@${msg_file}" \
  ${endpoint}

# Delete the temp file.
rm -fv "${msg_file}"
