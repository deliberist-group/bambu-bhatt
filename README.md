# bamboo-bhatt

[![pipeline status][pipeline_img]][commits]

A [Rocket.Chat][rocketchat] bot that reports build status from a Bamboo server in
the spirit of [Babu Bhatt](babu).

In reality, there is nothing in here that is specific to
[Atlassian Bamboo][bamboo].  We just use Bamboo on our team, so I liked the play
on words 💯.

## Rocket.Chat Links

- https://docs.rocket.chat/api/rest-api/methods/chat/postmessage

- https://docs.rocket.chat/api/rest-api/personal-access-tokens

## Running Rocket.Chat Locally

You can either run from source:

```bash
# https://linoxide.com/linux-how-to/install-rocket-chat-ubuntu-16-04-docker/
cd Rocket.Chat
docker-compose up
```

Or run from the Docker images:

```bash
# Starting...
docker run --name db -d mongo:4.0 --smallfiles --replSet rs0 --oplogSize 128
docker exec -ti db mongo --eval "printjson(rs.initiate())"
docker run --name rocketchat -p 80:3000 --link db --env ROOT_URL=http://localhost --env MONGO_OPLOG_URL=mongodb://db:27017/local -d rocket.chat
docker image ls
docker container ls
echo "Open:  http://localhost/"

# Stopping...
docker container stop rocketchat
docker container stop db
docker container rm -f rocketchat
docker container rm -f db
docker container ls -a
```

[pipeline_img]: https://gitlab.com/deliberist/bambu-bhatt/badges/main/pipeline.svg
[commits]: https://gitlab.com/deliberist/bambu-bhatt/-/commits/main
[rocketchat]: https://rocket.chat/
[babu]: https://seinfeld.fandom.com/wiki/Babu_Bhatt
[bamboo]: https://www.atlassian.com/software/bamboo
